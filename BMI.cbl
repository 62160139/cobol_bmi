       IDENTIFICATION DIVISION. 
       PROGRAM-ID. BMI.
       AUTHOR. PISIT.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  BMI-TEST       PIC X(50) VALUE SPACE.
       01  WEIGHT         PIC 999V9 VALUE ZEROES.
       01  HEIGHT         PIC 999V9 VALUE ZEROES.
       01  BMI-RESULT     PIC 99V9.

       PROCEDURE DIVISION.
           DISPLAY "Enter your weight : - "
           ACCEPT WEIGHT 
           DISPLAY "Enter your height : - "
           ACCEPT HEIGHT 
           COMPUTE BMI-RESULT = WEIGHT / ((HEIGHT/100)*(HEIGHT/100))
           EVALUATE TRUE 
              WHEN BMI-RESULT < 18.5 MOVE "UNDERWEIGHT" TO BMI-TEST
              WHEN BMI-RESULT >= 18.5 AND BMI-RESULT < 22.9 
               MOVE "AVERAGE" TO BMI-TEST
              WHEN BMI-RESULT > 23.0 AND BMI-RESULT <= 24.9
                MOVE "OVERWEIGHT" TO BMI-TEST 
              WHEN BMI-RESULT > 25.0 AND BMI-RESULT <= 29.9
                MOVE "OBESITY" TO BMI-TEST
              WHEN BMI-RESULT > 30 MOVE "DANGEROUS-OBESITY" TO BMI-TEST
           END-EVALUATE 
           DISPLAY "BMI : - " BMI-RESULT 
           DISPLAY "BMI-TEST : - " BMI-TEST
           GOBACK 
           .
